#!/bin/bash
# Wed May 22 16:14:58 EDT 2019

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 instance_name  instance_IP"
    exit 1
fi

gcloud compute instances create "$1" \
        --machine-type n1-standard-1 \
        --image-project=ubuntu-os-cloud \
        --image=ubuntu-1604-xenial-v20190514 \
        --network ip-failover \
        --can-ip-forward \
        --private-network-ip="$2" \
        --scopes compute-rw \
        --zone us-east1-b \
        --metadata 'startup-script=apt-get update'

exit 0
