**Implementation**
---
There are three main parts of this system:

1. **Data synchronization between nodes by DRBD.**
2. **Exporting directory for remote access by NFS.**
3. **Control health of nodes and services (DRBD, NFS) on them with Heartbeat.**


Two nodes are acting in active/passive mode and data can be accessed only on one of them which at this time primary.

DRBD replicates data from primary node to the secondary via TCP/IP connection.

It uses **Protocol C** which means that write operation to local partition considered completed only if write to disks finished on both nodes.

Data can be accessed by users only on primary node.

If two drbd nodes become active then it will cause **split-brain** issue where both synced partitions can me modified.
 
 
Heartbeat connects to other node in cluster and check does it alive. 

If master node goes down Heartbeat on slave node detects it and startups all services defined in config file `/etc/heartbeat/haresources` in a left-to-right order.

In our case it will first run custom script `takeOverFloatingIP` to move virtual IP to slave node, after mounts DRBD partition and then starts NFS server.

Now slave node becomes primary in a cluster and will take all user data.


When master node starts running again Heartbeat asks slave node to give up all resources.

Slave node firstly stops resources in file `/etc/heartbeat/haresources` in right-to-left order: NFS -> unmount DRBD partition -> `takeOverFloatingIP`.

After master node runs services in following order

1. `takeOverFloatingIP` - move virtual IP to master node 
2. Mount DRBD partition - now data is accessible 
3. NFS server - export directory for remote access

At the end master node owns all services and user data is going to him.


Some pictures:

1. ![Both nodes master and slave are alive](https://gitlab.com/haqler/nfs-balancer/blob/master/images/two_nodes_alive.jpg)
2. ![Master node is dead](https://gitlab.com/haqler/nfs-balancer/blob/master/images/master_node_dead.jpg)



**Installation**
---
Clone this repository `git clone https://haqler@gitlab.com/haqler/nfs-balancer.git`

Type `cd nfs-balancer` and `./pkg-install.sh`.

Script will install packages and build DRBD from source code.


**Configuration**
---

**Network**

Copy *interfaces* to `/etc/network/interfaces` and reboot system to create new network interface with virtual IP.


**DRBD**

*Do this steps first on master and after on slave node:*

After building DRBD and utils from source code run in command line `modprobe drbd` which will load DRBD kernel module.

Activate drbd service `service drbd start`

Copy *nfs.res* file to `/etc/drbd.d/` directory.

Then run create DRBD resource by typing `drbdadm create-md nfs`. It should response that resource successfully created.

Type `drbdadm up nfs` to activate resource

*Only on master node:*

`drbdadm -- --overwrite-data-of-peer primary nfs --force` to make this node primary and assume his data is actual.

After synchronization should start, you can check status of DRBD nodes with command `drbdadm status`. 

It should show that master node is primary, slave node is secondary. Data on master should be UpToDate and on slave node UpToDate or Sync.

For example

    # drbdadm status
    nfs role:Primary
      disk:UpToDate
      slave-nfs role:Secondary
        peer-disk:UpToDate

After synchronization complete type `mkfs.ext4 /path/to/drbd/partition` to create file system on DRBD partition specified in `/etc/drbd.d/nfs.res` file.

**NFS**

Copy *exports* to `/etc/exports`


**Heartbeat**

Copy this file to the following destinations

*authkeys* - `/etc/heartbeat/`

*ha.cf* - `/etc/heartbeat/`

*haresources* - `/etc/heartbeat/`

*takoverIP.sh* - `/etc/heartbeat/`

*takeOverFloatingIP* - `/etc/heartbeat/resource.d/`

And then start heartbeat `service heartbeat start`.

To see log file run `tail -f /var/run/ha-debug`

If everything works setup heartbeat to run on startup `update-rc.d heartbeat defaults`

**Scripts**
---

pkg-install.sh
---
This script installs all needed packages for `DRBD` and `hearbeat`.

DRBD and its utils are builded from source code on gitlab [https://github.com/linbit](https://github.com/linbit).

If during installation of packages occur some error script will stop immediately.


config/heartbeat/resource.d/takeOverFloatingIP
---
This is LSB compliant script that heartbeat uses for moving floating IP to the instance.

Actually script calls another `takoverIP.sh` script to reassign IP.

Script must be placed in the `/etc/heartbeat/resource.d/` directory !


 config/heartbeat/takoverIP.sh
 ---
Main job of this script just move IP to the current instance.

It uses `gcloud` utility and makes API calls to route traffic to the instance.

This is one of the suggested approaches by Google Cloud to simulate floating IP mechanism as in the other cloud providers, for example AWS or DigitalOcean.

Here is link to article [Best practices for floating IP addresses](https://cloud.google.com/solutions/best-practices-floating-ip-addresses) (Option 4: Failover using routes API calls)

Place script to the directory `/etc/heartbeat/`.


create-instance.sh
---
Small automation of creating new Ubuntu xenial instances with one CPU and 3.75GB of RAM in us-east1-b zone.

It takes as arguments two values: instance name and its IP.


**Config files**
---

drbd/nfs.res
---
This is DRBD config file which defines new resource.

If you will use new instances for your environment then adjust IP address, partitions and host names.

Put this file to the directory `/etc/drbd.d/`


heartbeat/authkeys
---
File contains password and auth method for heartbeat.

Put this file to the directory `/etc/heartbeat/`


heartbeat/ha.cf
---

Node names, there IP, logging and times for health checks defined here.

Change hostnames and IPs to your own servers.

Put it into `/etc/heartbeat/` directory.


heartbeat/haresources
---
File contains name of the instance that will be used by default and services to monitor.

Configure server hostname and drbd disk if needed.

Put this file to the `/etc/heartbeat/` directory.


nfs/exports
---
NFS config file to define exported directories.

Change IP to your client server.


network/interfaces
---

This is interface configuration of static IP that will be used to connect by clients for Ubuntu xenial server.

Configure interface name (eth0, ens4, ...) and static IP for your requirements.

Put content of this file to the `/etc/network/interfaces`





