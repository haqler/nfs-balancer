1. Setup active/active (n-way multi master replication) - `DONE`
2. Configure VPN for to access user project VPC from outside
3. Setup SSL for replication between ldap nodes
4. Deploy Elastifile as HA nfs storage - `DONE`