**Overview**
--
We will setup two ldap servers in active/active replication mode where both nodes can accept writes and replicates them to each other.

If one node fails second will serve all requests.

As far first node start running it will get all updated data from second node.

**Install LDAP**
---

*Do this on both nodes*

    apt-get update

    apt-get install slapd ldap-utils phpldapadmin

During installation the user will be asked to enter the admin password for ldap.

Here
    
`slapd` - main ldap service

`ldap-utils` - utils to modify data and ldap configs

`phpldapadmin` - simple web interface to work with ldap data


**Configure LDAP**
--
*Do this on both nodes*

Reconfigure ldap by running command
    
    dpkg-reconfigure slapd

Enter 
    
`Domain name` - it can be your existing domain name or some test domain, for example, test-ldap.com. It used to build your `base DN (base distinguished name)` - the root of Data Information Tree.

`Organisation name` - organisation name

`Administrator password` - admin password.

`Database backend type` - MDB by default

`Remove database when slapd purged` - NO

`Move old database` - YES. It will be copied to `/var/backups/`

`Allow LDAPv2 protocol` - NO. By default ldap uses LDAPv3


After edit file `/etc/ldap/ldap.conf`. Uncomment and update values for

`BASE` - base DN you've entered in slapd configuration before. For example `dc=test-ldap,dc=com`

`URI` - your ldap uri, for example `ldap://10.142.0.18`


To configure phpldapadmin edit file `/etc/phpldapadmin/config.php`

Update values for

`$servers->setValue('server','name','Your Ldap Server Name');` - enter ldap server name which will be displayed after login in phpldapadmin

`$servers->setValue('server','base',array('dc=test-ldap,dc=com'));` - enter ldap base DN, for example `dc=test-ldap,dc=com`

`$servers->setValue('login','bind_id','cn=admin,dc=test-ldap,dc=com');` - username to login, for example `cn=admin,dc=test-ldap,dc=com` (this is admin user)

If firewall is enabled allow http port

    ufw allow 80

Now restart apache

    service apache2 restart

Open your browser and type `your_server_id/phpldapadmin`, it will navigate you to phpldapadmin login page.

As user name use value entered in phpldapadmin config(`cn=admin,dc=test-ldap,dc=com`). By default it's admin user.

And password that was entered during slapd configuration.


Now we have two ldap servers working. Let's start replication configuration

*Do on both nodes*

Firstly setup provider (master) part of ldap doing this

    ldapmodify -Y EXTERNAL -H ldapi:// -f provider_part.ldif
    
It will create database, setup indexing and load `syncprov` module that responsible for replication on provider side.


*Do on first node*

Setup consumer (slave) part of ldap

    ldapmodify -Y EXTERNAL -H ldapi:// -f consumer_part_firstnode.ldif
    
*Do on second node*

    ldapmodify -Y EXTERNAL -H ldapi:// -f consumer_part_secondnode.ldif

This files configures
    
`provider url` - url to get data from

`data tree to sync` - which data tree should be replicated

`read access to provider database`

`credentials to connect` - user and password

`replication mode` - in our case it's `refreshAndPersist`. In this mode consumers connect to provider, get updates and keeps connection for further instructions from provider

`load module olcSyncRepl` - this module is responsible for replication on consumer (slave) side
    
Now replication should work. Test it by creating some entries in phpldapadmin console.


**Useful links**
--
[Ubuntu documentation for LDAP](https://help.ubuntu.com/lts/serverguide/openldap-server.html.en)

[Amazing LDAP replication explanation on zytrax](http://www.zytrax.com/books/ldap/ch7/)
    
