**LDAP TEST CASES**
---

*How to create organization in phpldapadmin web ldap console*

1. Login under admin
2. Select ‘create new entry’
3. Select ‘Generic: Organisational Unit’
4. Write name of organization
5. Press ‘commit’

*How to create group in organization*
1. Login under admin
2. Select you organization
3. Select ‘create a child entry’
4. Select ‘Generic: Posix Group’
5. Type name of group
6. Press ‘commit

*How to create a new user in group*
1. Login under admin
2. Select your organization
3. Select your group
4. Select ‘create a child entry’
5. Select ‘Generic: User Account’
6. Fill necessary fields
7. Press ‘commit’

In our test case organization is **nfs-users** and **group work**.

*Replication from first master node to second*
1. Login to phpldapadmin on first master node
2. Create new user, for example **alex**.
3. Check on second node user alex, it must be there.

*Replication from second master node to first*
1. Login to phpldapadmin on second master node
2. Create new user, for example **anne**.
3. Check on first node user anne, it must be there.

*Login with stopped first master node*
1. Shutdown first master node
2. Login under some existing ldap user: `su alex`
3. Login must be successful

*Login with stopped second master node*
1. Shutdown second node
2. Login unser some existing ldap user: `su alex`
3. Login must be successful

*Make changes in ldap when one node stopped*
1. Shutdown first node
2. Login under some existing ldap user: `su alex`
3. Login must be successful
4. Change user password: passwd
5. Password should be changed successfully
6. Logout
7. Start first node and after stop second node
8. Login under your user again: `su alex`
9. Type your new password
10. Login must be successful
