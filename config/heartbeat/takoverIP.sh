#!/bin/bash
# Wed May 22 19:15:48 UTC 2019

echo "Start routing: $(date)"
gcloud compute routes delete floating --quiet
gcloud compute routes create floating --destination-range 10.190.1.1/32 --network ip-failover --priority 500 --next-hop-instance-zone us-east1-b --next-hop-instance "$(uname -n)" --quiet 
echo "Finish routing: $(date)"

exit 0

