resource nfs {
   device    /dev/drbd0;
   disk    /dev/vg_nfs/lv_nfs_drbd;
   meta-disk    internal;


   net {
      protocol    C;
   }


   connection-mesh {
      hosts    master-nfs slave-nfs;
   }


   on master-nfs {
      address    10.142.0.100:7790;
      node-id    0;
   }


   on slave-nfs { 
      address    10.142.0.101:7790;
      node-id    1;
   }
}
