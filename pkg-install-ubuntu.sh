#!/bin/bash
# nizamiyi
# Thu May 16 06:03:23 UTC 2019

PKGS='nfs-kernel-server ntp ntpdate build-essential libelf-dev make flex clitest po4a xsltproc pcs heartbeat drbd-utils automake'
DRBD_GIT_URL='https://github.com/LINBIT/drbd-9.0.git'
DRBD_UTILS_GIT_URL='https://github.com/LINBIT/drbd-utils.git'
WDIR=~/

function info_msg() {
    local NC='\033[0m'
    local GREEN='\033[0;32m'

    echo -e "\n${GREEN}${1}${NC}"
    return 0
}

function error_msg() {
    local NC='\033[0m'
    local RED='\033[0;31m'

    echo -e "\n${RED}${1}${NC}"
    return 0
}

info_msg 'Updating repositories.'
apt-get update

for pkg in $PKGS; do
        info_msg "Install package: ${pkg}"
        apt-get install "$pkg"

        if [ "$?" -ne 0 ]; then
                error_msg 'Error during package installation. Exiting...'
                exit 1
        fi
done



info_msg "Clone DRBD and utils source code to ${WDIR}"
git -C "${WDIR}" clone "$DRBD_GIT_URL"
git -C "${WDIR}" clone "$DRBD_UTILS_GIT_URL"

info_msg 'Compile and install DRBD'
cd "${WDIR}/drbd-9.0" && make clean all && make install

if [ "$?" -ne 0 ]; then
    error_msg 'Cannot install DRBD.'
    exit 1
fi


info_msg 'Compile and install DRBD'
cd "${WDIR}/drbd-utils" && ./autogen.sh && ./configure --prefix=/usr/local/drbd-utils --sysconfdir=/etc && make clean all && make install

if [ "$?" -ne 0 ]; then
    error_msg 'Cannot install DRBD utils.'
    exit 1
fi

exit 0
