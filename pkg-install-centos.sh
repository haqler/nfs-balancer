#!/bin/bash
# nizamiyi
# Mon Jun 24 15:40:05 EDT 2019

PKGS='nfs-utils ntp ntpdate kernel kernel-devel kernel-headers gcc make gcc-c++ elfutils-devel flex po4a libxslt libxslt-devel rpm-build pacemaker pcs psmisc policycoreutils-python automake autoconf'
DRBD_GIT_URL='https://github.com/LINBIT/drbd-9.0.git'
DRBD_UTILS_GIT_URL='https://github.com/LINBIT/drbd-utils.git'
WDIR=~/

function info_msg() {
    local NC='\033[0m'
    local GREEN='\033[0;32m'

    echo -e "\n${GREEN}${1}${NC}"
    return 0
}

function error_msg() {
    local NC='\033[0m'
    local RED='\033[0;31m'

    echo -e "\n${RED}${1}${NC}"
    return 0
}


for pkg in $PKGS; do
        info_msg "Install package: ${pkg}"
        yum install "$pkg"

        if [ "$?" -ne 0 ]; then
                error_msg 'Error during package installation. Exiting...'
                exit 1
        fi
done



info_msg "Clone DRBD and utils source code to ${WDIR}"
cd "${WDIR}"
git clone "$DRBD_GIT_URL"
git clone "$DRBD_UTILS_GIT_URL"

info_msg 'Compile and install DRBD'
cd "${WDIR}/drbd-9.0" && make clean all && make install

if [ "$?" -ne 0 ]; then
    error_msg 'Cannot install DRBD.'
    exit 1
fi


info_msg 'Compile and install DRBD'
cd "${WDIR}/drbd-utils" && ./autogen.sh && ./configure --prefix=/usr/local/drbd-utils --sysconfdir=/etc && make clean all && make install

if [ "$?" -ne 0 ]; then
    error_msg 'Cannot install DRBD utils.'
    exit 1
fi

exit 0
